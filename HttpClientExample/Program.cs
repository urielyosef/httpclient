﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HttpClientExample
{
    public class Program
    {
        private readonly HttpClient _client = CreateHttpClient();

        public static async Task Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        private async Task Post(object bodyValue, string uri)
        {
            string jsonBodyValue = JsonConvert.SerializeObject(bodyValue);
            StringContent bodyContent = 
                new StringContent(jsonBodyValue, Encoding.UTF8, "application/json");

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
            request.Content = bodyContent;

            var response = await _client.SendAsync(request);

            Console.WriteLine($"IsSuccessStatusCode: {response.IsSuccessStatusCode}," +
                              $" Reason: {response.ReasonPhrase}");
        }

        private static HttpClient CreateHttpClient()
        {
            HttpClientHandler clientHandler = new HttpClientHandler
            {
                UseDefaultCredentials = true,
                ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, certChain, policyErros) => true
            };
            var client = new HttpClient(clientHandler);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }
    }
}